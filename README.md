This is a quick and dirty python script to get you login to Google Wifi after your Linux (or Mac OSX) box is associated with the access point. It has been tested at Starbucks that has switched to Google Wi-Fi (as opposed to attwifi).

Instead of launching the browser, execut this python script to get through the captive login page. Doing so brings some benefits of not seeing promotional content before hitting your desired URL.

It is relying on wget at this point, but I intent to replace everything with requests. The html doesn't give base url so I have to rely on info in HTTP header of the first GET.

Requirement: python and the requests module. If requests is not installed, do sudo easy_install requests or sudo pip install requests.

