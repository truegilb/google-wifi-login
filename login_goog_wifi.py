#!/usr/bin/python
import requests
import sys
import re

AT_SBUX = 1
TEST_URL = "http://www.github.com" # don't use google.com as it's likely whitelisted
# test like this:  cat session.script|./argv[0].py

if (AT_SBUX) :
  r = requests.head( TEST_URL, allow_redirects=False )
  print r.headers
  # if not redirect'ed then we should be connected (rough test)
  # http://jigsaw.w3.org/HTTP/300/
  #
  if (( r.status_code / 100 ) * 100 != 300):
    print "looks like you are already connected. exiting"
    sys.exit()
  else:
    # we should have the location (where we post the form here)
    #
    line = r.headers[ 'location' ]  
    print line
#  line = sys.stdin.read()
else:
  line = sys.stdin.read()

# parse for hostname and parameters to submit
match = re.search( 'Location.*(http.*)\/.*\?(.*)', line)
if match is not None:
  url = match.group(1)
  print url
  # params = match.group(2)
  # print params
else:
  print "don't know what URL to proceed"
  exit

# <form action="/submit" method="post">
# r.content
# match = re.search( 'form action\=\"(.*)\"', r.content)

# this is the test file, but will be what returns from GET
#

if (AT_SBUX) :
  captive_page = r.content
else:
  captive_page = open( 'index.html', 'r').read()

# need to look for the form name
#
match = re.search( 'form action\=\"(.*?)\"', captive_page ) # non-greedy grab
if match is not None:
  form = match.group(1) # the form name 
  print form
  #  print url + form + '?' + params

# process the params - from the form
#formtext = re.search( 'input.*name\=(.*)value\=(.*)', captive_page)
# do multi-line match
#
formtext = re.findall( 'form(.*)form', captive_page, re.DOTALL)
if formtext is not None:
#  print formtext[0]
  lines = formtext[0].split('\n')
  params = {}
  for i in lines:
    # find all the parameters of the form
    match = re.search( 'input.*name\=\"(.*)\".*value\=\"(.*)\"', i)
    if match is not None:
      params[ match.group(1) ] = match.group(2)

  print params

else:
  print 'not found'

print url+form, params
if (AT_SBUX):
  r = requests.post( url+form, params) 
  print r.content
#
